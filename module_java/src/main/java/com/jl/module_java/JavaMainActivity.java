package com.jl.module_java;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.jl.module_kotlin_service.KotlinCallService;

import androidx.appcompat.app.AppCompatActivity;

import lib.lh.router.JRouter;
import lib.lh.router.RouterPath;


@Route(path = RouterPath.MODULE_JAVA_JAVA_ACTIVITY)
public class JavaMainActivity extends AppCompatActivity {


    @Autowired
    public String describe;

    @Autowired
    public String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JRouter.instance().inject(this);
        setContentView(R.layout.java_activity_main);
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KotlinCallService kotlinCallService = JRouter.instance().navigation(KotlinCallService.class);
                kotlinCallService.remoteCall("$$我是Kotlin模块的调用代码$$");
            }
        });
        Toast.makeText(this, String.format("名称：%s，描述：%s", name, describe), Toast.LENGTH_SHORT).show();
    }
}