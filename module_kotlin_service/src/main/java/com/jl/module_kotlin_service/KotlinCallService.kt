package com.jl.module_kotlin_service

import com.alibaba.android.arouter.facade.template.IProvider

interface KotlinCallService : IProvider {
    fun remoteCall(message: String)
}