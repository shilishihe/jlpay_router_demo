import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boost/flutter_boost_app.dart';
import 'package:module_flutter_news/china_news.dart';
import 'package:module_flutter_news/cs_news.dart';
import 'package:module_flutter_news/page_not_found.dart';
import 'package:module_flutter_news/root_init.dart';
import 'package:module_flutter_news/routers.dart';
import 'package:module_flutter_news/sh_news.dart';
import 'package:module_flutter_news/sz_news.dart';
import 'package:flutter_boost/boost_navigator.dart';

class Index extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _IndexState();
  }
}

class _IndexState extends State<Index> {
  static Map<String, FlutterBoostRouteFactory> routerMap = {
    '/': (settings, uniqueId) {
      return PageRouteBuilder<dynamic>(
          settings: settings, pageBuilder: (_, __, ___) => RootInit());
    },
    Routers.FLUTTER_MODULE_CHINA_NEWS: (settings, uniqueId) {
      return PageRouteBuilder<dynamic>(
          settings: settings,
          pageBuilder: (_, __, ___) => ChinaNews(
                args: settings.arguments,
                message: "我是消息",
                uniqueId: uniqueId,
              ));
    },
    Routers.FLUTTER_MODULE_PAGE_NOT_FOUND: (settings, uniqueId) {
      return PageRouteBuilder<dynamic>(
          settings: settings, pageBuilder: (_, __, ___) => PageNotFound());
    },
    Routers.FLUTTER_MODULE_SZ_NEWS: (settings, uniqueId) {
      return PageRouteBuilder<dynamic>(
          settings: settings, pageBuilder: (_, __, ___) => SZNews());
    },
    Routers.FLUTTER_MODULE_SH_NEWS: (settings, uniqueId) {
      return PageRouteBuilder<dynamic>(
          settings: settings, pageBuilder: (_, __, ___) => SHNews());
    },
    Routers.FLUTTER_MODULE_CS_NEWS: (settings, uniqueId) {
      /*
      // 单独跑的时候这段代码导航返回会有问题
      return CupertinoPageRoute<void>(
        builder: (BuildContext context) {
          return CSNews();
        },
      );*/
      return PageRouteBuilder<dynamic>(
          settings: settings, pageBuilder: (_, __, ___) => CSNews());
    },
  };

  Route<dynamic> routeFactory(RouteSettings settings, String uniqueId) {
    FlutterBoostRouteFactory func = routerMap[settings.name];
    if (func == null) {
      // 从Flutter push Native时候uniqueId参数为NULL（查看源码得出）
      if (uniqueId == null) {
        return null;
      }
      func = routerMap[Routers.FLUTTER_MODULE_PAGE_NOT_FOUND];
    }
    return func(settings, uniqueId);
  }

  @override
  void initState() {
    super.initState();
    runExceptNativeLogic();
  }

  // 是否单独运行调试Flutter代码
  void runExceptNativeLogic() {
    Future.delayed(Duration.zero, () {
      BoostNavigator.of().push(Routers.FLUTTER_MODULE_CHINA_NEWS);
    });
  }

  @override
  Widget build(BuildContext context) {
    return FlutterBoostApp(
      routeFactory,
      appBuilder: (home) {
        return appBuilder(home);
      },
    );
  }

  Widget appBuilder(Widget home) {
    return MaterialApp(
      home: home,
    );
  }
}
