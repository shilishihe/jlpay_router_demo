import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:module_flutter_news/routers.dart';
import 'package:flutter_boost/boost_navigator.dart';

class SZNews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('深圳新闻'),
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              BoostNavigator.of().pop();
            },
          );
        }),
      ),
      body: Center(
        child: TextButton(
          child: Text('跳转到上海新闻'),
          onPressed: () {
            BoostNavigator.of().push(Routers.FLUTTER_MODULE_SH_NEWS);
          },
        ),
      ),
    );
  }
}
