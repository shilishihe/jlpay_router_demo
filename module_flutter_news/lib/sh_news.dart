import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boost/boost_navigator.dart';
import 'package:module_flutter_news/routers.dart';

class SHNews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('上海新闻'),
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              BoostNavigator.of().pop();
            },
          );
        }),
      ),
      body: Center(
        child: TextButton(
          child: Text('跳转到长沙新闻'),
          onPressed: () {
            BoostNavigator.of().push(Routers.FLUTTER_MODULE_CS_NEWS);
            // CSNews
          },
        ),
      ),
    );
  }
}
