import 'package:flutter/material.dart';
import 'package:flutter_boost/boost_navigator.dart';
import 'package:module_flutter_news/routers.dart';

class ChinaNews extends StatefulWidget {
  final Map args;
  final String message;
  final String uniqueId;

  const ChinaNews({Key key, this.args, this.message, this.uniqueId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ChinaNewsState();
  }
}

class ChinaNewsState extends State<ChinaNews> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("中国新闻"),
        brightness: Brightness.dark,
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("具有一定规模的App通常有一套成熟通用的基础库，尤其是阿里系App，一般需要依赖很多体系内的基础库。"),
            Text.rich(TextSpan(children: <TextSpan>[
              TextSpan(text: "\n传递过来的args = "),
              TextSpan(
                  text: "${widget?.args}",
                  style: TextStyle(color: Colors.blue)),
            ])),
            Text.rich(TextSpan(children: <TextSpan>[
              TextSpan(text: "\n传递过来的message = "),
              TextSpan(
                  text: "${widget?.message}",
                  style: TextStyle(color: Colors.blue)),
            ])),
            Text.rich(TextSpan(children: <TextSpan>[
              TextSpan(text: "\n传递过来的uniqueId = "),
              TextSpan(
                  text: "${widget?.uniqueId}",
                  style: TextStyle(color: Colors.blue)),
            ])),
            TextButton(
              onPressed: () {
                BoostNavigator.of().push(Routers.NATIVE_MODULE_MAIN,
                    arguments: {
                      "describe":
                          "Flutter Engine负责线程管理，Dart VM状态管理和Dart代码加载等工作。",
                      "name": "阿里FlutterBoost框架"
                    },
                    withContainer: true);
              },
              child: Text(
                "跳转到MODULE_MAIN",
                style: TextStyle(fontSize: 18),
              ),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith((states) {
                if (states.contains(MaterialState.pressed)) {
                  return Colors.blue[200];
                }
                return null;
              })),
            ),
            TextButton(
              onPressed: () {
                BoostNavigator.of().push(Routers.FLUTTER_MODULE_SZ_NEWS);
              },
              child: Text(
                "跳转到深圳新闻",
                style: TextStyle(fontSize: 18),
              ),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith((states) {
                if (states.contains(MaterialState.pressed)) {
                  return Colors.blue[200];
                }
                return null;
              })),
            )
          ],
        ),
      ),
    );
  }
}
