import 'package:flutter/material.dart';

class PageNotFound extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("页面未找到"),
      ),
      body: Center(
        child: Text(
          "404",
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
