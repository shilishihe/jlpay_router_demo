import 'package:module_flutter_news/fit_utils.dart';

/// * @Author: lihe
/// * @Created at: 2020/11/11
/// * @Email: shilishihe@gmail.com
/// * @Company: 嘉联支付
/// * 全局初始化

class Global {
  //是否为release版
  static bool get isRelease => const bool.fromEnvironment("dart.vm.product");

  //初始化全局信息，会在APP启动时执行
  static void init() {
    FitUtils.initialize(); //屏幕适配
  }
}
