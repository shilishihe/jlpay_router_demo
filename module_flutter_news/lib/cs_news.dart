import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boost/boost_navigator.dart';

class CSNews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text('长沙新闻'),
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              BoostNavigator.of().pop();
            },
          );
        }),
      ),
      body: Center(
        child: TextButton(
          child: Text('长沙新闻POP'),
          onPressed: () {
            BoostNavigator.of().pop();
          },
        ),
      ),
    );
  }
}
