class Routers {
  static String NATIVE_MODULE_JAVA = "/module_java/java_activity";
  static String NATIVE_MODULE_MAIN = "/module_demo/main_activity";
  static String FLUTTER_MODULE_CHINA_NEWS = "/module_flutter_news/china_news";
  static String FLUTTER_MODULE_PAGE_NOT_FOUND =
      "/module_flutter_news/page_not_found";
  static String FLUTTER_MODULE_SZ_NEWS = "/module_flutter_news/page_sz_news";
  static String FLUTTER_MODULE_SH_NEWS = "/module_flutter_news/page_sh_news";
  static String FLUTTER_MODULE_CS_NEWS = "/module_flutter_news/page_cs_news";
}
