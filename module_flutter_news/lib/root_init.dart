import 'package:flutter/material.dart';

import 'global.dart';

class RootInit extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RootInitRootState();
  }
}

class _RootInitRootState extends State<RootInit> {
  @override
  void initState() {
    super.initState();
    Global.init();
    print("初始化根路由页面");
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox();
  }
}
