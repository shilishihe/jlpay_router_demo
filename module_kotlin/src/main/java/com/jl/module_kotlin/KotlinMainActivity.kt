package com.jl.module_kotlin

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.appcompat.app.AppCompatActivity
import com.alibaba.android.arouter.facade.annotation.Route
import lib.lh.router.JRouter
import lib.lh.router.RouterPath

@Route(path = RouterPath.MODULE_KOTLIN_KOTLIN_ACTIVITY)
class KotlinMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.kotlin_activity_main)
        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
        }
        JRouter.instance().inject(this)
    }


}