package com.jl.module_kotlin

import android.content.Context
import android.widget.Toast
import com.alibaba.android.arouter.facade.annotation.Route
import com.jl.module_kotlin_service.KotlinCallService

@Route(path = "/KotlinCallService/remoteCall")
class KotlinCallServiceImpl : KotlinCallService {

    lateinit var mContext: Context

    override fun remoteCall(message: String) {
        Toast.makeText(mContext, "Hello $message", Toast.LENGTH_SHORT).show()
    }

    override fun init(context: Context?) {
        mContext = context!!
    }
}