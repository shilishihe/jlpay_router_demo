package lib.lh.router;

/**
 * 路由路径
 */
public interface RouterPath {

    String MODULE_JAVA_JAVA_ACTIVITY = "/module_java/java_activity";
    String MODULE_DEMO_MAIN_ACTIVITY = "/module_demo/main_activity";
    String MODULE_KOTLIN_KOTLIN_ACTIVITY = "/module_kotlin/kotlin_activity";
    String MODULE_DEMO_USER_CENTER = "/module_demo/user_center_activity";
    String MODULE_DEMO_USER_LOGIN = "/module_demo/user_login_activity";
    String MODULE_DEMO_CONSTRAINT_LAYOUT_ACTIVITY = "/module_demo/constraint_layout_activity";
    String MODULE_ROUTER_H5_ACTIVITY = "/module_router/h5_activity";
    String MODULE_ROUTER_LOSS_ACTIVITY = "/module_router/loss_activity";
    String MODULE_ROUTER_FLUTTER_DISPATCH = "/module_router/flutter_dispatch";
    String MODULE_ROUTER_DEFAULTJSONSERVICEIMPL = "/module_router/DefaultJsonServiceImpl";
    String NATIVE_DEFAULT_DEGRADE_SERVICE = "/default/degrade_service";
    String MODULE_NOT_FOUND = "/module_router/not_found";
    String MODULE_FLUTTER_NEWS_CHINA_NEWS = "/module_flutter_news/china_news";

    static String fullSchemePath(String corePath) {
        return String.format("%s://%s%s", JRouter.instance().getScheme(), JRouter.instance().getHost(), corePath);
    }

    static String schemePathHead() {
        return String.format("%s://%s", JRouter.instance().getScheme(), JRouter.instance().getHost());
    }

}
