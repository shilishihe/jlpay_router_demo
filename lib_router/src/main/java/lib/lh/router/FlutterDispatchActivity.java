package lib.lh.router;

import android.app.Activity;

import com.alibaba.android.arouter.facade.annotation.Route;

@Route(path = RouterPath.MODULE_ROUTER_FLUTTER_DISPATCH, extras = RouterExtrasFlag.FLAG_FLUTTER)
public class FlutterDispatchActivity extends Activity {
}