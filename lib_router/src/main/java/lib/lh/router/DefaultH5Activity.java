package lib.lh.router;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import com.alibaba.android.arouter.facade.annotation.Route;

@Route(path = RouterPath.MODULE_ROUTER_H5_ACTIVITY)
public class DefaultH5Activity extends Activity {

    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_webview);
        webview = findViewById(R.id.webview);
        String url = getIntent().getStringExtra("url");
        webview.loadUrl(url);
    }
}
