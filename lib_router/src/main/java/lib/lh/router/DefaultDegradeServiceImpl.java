package lib.lh.router;

import android.content.Context;
import android.os.Bundle;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.service.DegradeService;
import com.google.gson.Gson;

import java.util.HashMap;

@Route(path = RouterPath.NATIVE_DEFAULT_DEGRADE_SERVICE)
public class DefaultDegradeServiceImpl implements DegradeService {

    private Context context;
    private Gson gson;

    @Override
    public void onLost(Context context, Postcard postcard) {
        boolean openFlutterBoost = JRouter.instance().isOpenFlutterBoost();
        if (openFlutterBoost) {
            Bundle mBundle = postcard.getExtras();
            String argsValue = mBundle.getString(JRouter.FLUTTER_ROUTER_ARGUMENTS, "");
            HashMap map = gson.fromJson(argsValue, HashMap.class);
            JRouter.instance()
                    .build(RouterPath.MODULE_ROUTER_FLUTTER_DISPATCH)
                    .withObject(JRouter.FLUTTER_ROUTER_ARGUMENTS, map)
                    .withString(JRouter.FLUTTER_ROUTER_NAME, postcard.getPath())
                    .navigation(this.context);
        } else {
            JRouter.instance().build(RouterPath.MODULE_ROUTER_LOSS_ACTIVITY).navigation();
        }
    }

    @Override
    public void init(Context context) {
        this.context = context;
        gson = new Gson();
    }
}
