package lib.lh.router;

import android.content.Context;
import android.os.Bundle;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Interceptor;
import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.template.IInterceptor;
import com.google.gson.Gson;
import com.idlefish.flutterboost.FlutterBoost;

import java.util.HashMap;
import java.util.Map;

// 容器化路由拦截器
@Interceptor(priority = 1, name = "容器化路由拦截器")
public class ContainerRouterIntercepter implements IInterceptor {

    private Context context;
    private Gson gson;

    @Override
    public void process(Postcard postcard, InterceptorCallback callback) {
        int routerFlage = postcard.getExtra();
        int flagModuleFlutter = routerFlage & RouterExtrasFlag.FLAG_FLUTTER;
        int flagModuleWeex = routerFlage & RouterExtrasFlag.FLAG_WEEX;
        if (flagModuleFlutter != 0) {
            callback.onInterrupt(new RuntimeException("需要处理Flutter路由的跳转"));
            Bundle mBundle = postcard.getExtras();
            String routerName = mBundle.getString(JRouter.FLUTTER_ROUTER_NAME, "");
            String argsValue = mBundle.getString(JRouter.FLUTTER_ROUTER_ARGUMENTS, "");
            Map<String, Object> map = gson.fromJson(argsValue, HashMap.class);
            FlutterBoost.instance().open(routerName, map);
        } else if (flagModuleWeex != 0) {
            callback.onInterrupt(new RuntimeException("需要处理Weekx路由的跳转"));
            // 跳转到Weekx模块
        } else {
            callback.onContinue(postcard);
        }
    }

    @Override
    public void init(Context context) {
        gson = new Gson();
        this.context = context;
    }
}
