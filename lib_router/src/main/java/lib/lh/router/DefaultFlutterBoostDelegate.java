package lib.lh.router;

import android.content.Intent;
import android.util.Log;

import com.idlefish.flutterboost.FlutterBoost;
import com.idlefish.flutterboost.FlutterBoostDelegate;
import com.idlefish.flutterboost.containers.FlutterBoostActivity;

import java.util.HashMap;
import java.util.Map;

import io.flutter.embedding.android.FlutterActivityLaunchConfigs;

/**
 * 默认的实现
 */
public class DefaultFlutterBoostDelegate implements FlutterBoostDelegate {

    private static final String TAG = "DefaultFBDelegate";

    @Override
    public void pushNativeRoute(String pageName, Map<String, Object> arguments) {
        Log.e(TAG, "pageName = " + pageName);
        Log.e(TAG, "arguments = " + arguments.toString());
        RouterEntrance.nav2(pageName, arguments);
    }

    @Override
    public void pushFlutterRoute(String pageName, String uniqueId, Map<String, Object> arguments) {
        Intent intent = new FlutterBoostActivity.CachedEngineIntentBuilder(FlutterBoostActivity.class, FlutterBoost.ENGINE_ID)
                .backgroundMode(FlutterActivityLaunchConfigs.BackgroundMode.opaque)
                .destroyEngineWithActivity(false)
                .uniqueId(uniqueId)
                .url(pageName)
                .urlParams(arguments == null ? new HashMap<>() : arguments)
                .build(FlutterBoost.instance().currentActivity());
        FlutterBoost.instance().currentActivity().startActivity(intent);
    }
}