package lib.lh.router;

import android.content.Context;

import com.alibaba.android.arouter.facade.Postcard;
import com.idlefish.flutterboost.FlutterBoost;

import java.util.HashMap;
import java.util.Map;

/**
 * 第二方的路由管理入口
 * 用来约束入参
 */
public class RouterEntrance {

    public static void nav2ModuleJavaActivity(Context context) {
        JRouter.instance()
                .build(RouterPath.MODULE_JAVA_JAVA_ACTIVITY)
                .withString("name", "我是名称")
                .withString("describe", "我是描述")
                .navigation(context);
    }

    public static void nav2(String router, Map<String, Object> args) {
        if (args.containsKey("pageNotFound") && args.get("pageNotFound").equals("Yes")) {
            JRouter.instance()
                    .build(RouterPath.MODULE_NOT_FOUND)
                    .withBoolean("pageNotFound", true)
                    .navigation(FlutterBoost.instance().currentActivity());
        } else {
            Postcard postcard = JRouter.instance().build(router);
            if (args != null) {
                for (String key : args.keySet()) {
                    postcard.withString(key, args.get(key).toString());
                }
            }
            postcard.navigation(FlutterBoost.instance().currentActivity());
        }


    }

}
