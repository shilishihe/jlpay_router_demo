package lib.lh.router;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavCallback;
import com.alibaba.android.arouter.facade.service.DegradeService;
import com.alibaba.android.arouter.launcher.ARouter;

/**
 * Scheme + Host + Module + Package + Business + params
 * jlrouter://m.jlpay.com/module_main/demo/activity1
 * jlrouter://m.jlpay.com/module_flutter_news?params={"router":"/home"}
 */
public class SchemeFilterActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Uri uri = getIntent().getData();
        Postcard postcard = ARouter.getInstance().build(uri);
        postcard.navigation(this, new NavCallback() {
            @Override
            public void onArrival(Postcard postcard) {
                finish();
            }

            @Override
            public void onLost(Postcard postcard) {
                DegradeService degradeService = ARouter.getInstance().navigation(DegradeService.class);
                if (null != degradeService) {
                    degradeService.onLost(SchemeFilterActivity.this, postcard);
                }
                finish();
            }
        });

    }
}
