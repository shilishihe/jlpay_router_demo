package lib.lh.router;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavigationCallback;
import com.alibaba.android.arouter.facade.template.ILogger;
import com.alibaba.android.arouter.launcher.ARouter;
import com.idlefish.flutterboost.FlutterBoost;

import java.util.concurrent.ThreadPoolExecutor;

import io.flutter.embedding.engine.FlutterEngine;

public final class JRouter {

    private ARouter aRouter = null;
    private static JRouter instance = null;
    private volatile static boolean hasInit = false;
    public volatile static ILogger logger;
    private boolean openLog;
    private boolean openDebug;
    private boolean autoInitFlutterEngine = false;
    public static final String FLUTTER_ROUTER_NAME = "flutter_router_name";
    public static final String FLUTTER_ROUTER_ARGUMENTS = "flutter_router_arguments";
    private Application application;
    private String scheme;
    private String host;
    private boolean openFlutterBoost;


    public String getScheme() {
        return scheme;
    }

    public String getHost() {
        return host;
    }

    public ARouter getaRouter() {
        return aRouter;
    }

    public static boolean isHasInit() {
        return hasInit;
    }

    public static ILogger getLogger() {
        return logger;
    }

    public boolean isOpenLog() {
        return openLog;
    }

    public boolean isOpenDebug() {
        return openDebug;
    }

    public boolean isAutoInitFlutterEngine() {
        return autoInitFlutterEngine;
    }

    public Application getApplication() {
        return application;
    }

    public static JRouter instance() {
        return instance;
    }

    private JRouter(Build build) {
        this.application = build.application;
        this.openLog = build.openLog;
        this.openDebug = build.openDebug;
        this.autoInitFlutterEngine = build.autoInitFlutterEngine;
        this.scheme = build.scheme;
        this.host = build.host;
        this.openFlutterBoost = build.openFlutterBoost;
        init(this.application);
    }

    private void init(Application application) {
        if (!hasInit) {
            if (BuildConfig.DEBUG) {
                if (this.openLog) {
                    ARouter.openLog();
                }
                if (this.openDebug) {
                    ARouter.openDebug();
                }
            }
            ARouter.init(application);
            aRouter = ARouter.getInstance();
            hasInit = true;
            if (openFlutterBoost) {
                FlutterBoost.instance().setup(application
                        , new DefaultFlutterBoostDelegate()
                        , FlutterEngine::getPlugins);
            }
        }
    }

    public  boolean isOpenFlutterBoost() {
        return openFlutterBoost;
    }


    public static synchronized void openDebug() {
        ARouter.openDebug();
    }

    public static boolean debuggable() {
        return ARouter.debuggable();
    }

    public static synchronized void openLog() {
        ARouter.openLog();
    }

    public static synchronized void printStackTrace() {
        ARouter.printStackTrace();
    }

    public static synchronized void setExecutor(ThreadPoolExecutor tpe) {
        ARouter.setExecutor(tpe);
    }

    public synchronized void destroy() {
        aRouter.destroy();
        hasInit = false;
    }

    /**
     * The interface is not stable enough, use 'ARouter.inject();';
     */
    @Deprecated
    public static synchronized void enableAutoInject() {
        ARouter.enableAutoInject();
    }

    @Deprecated
    public static boolean canAutoInject() {
        return ARouter.canAutoInject();
    }

    /**
     * The interface is not stable enough, use 'ARouter.inject();';
     */
    @Deprecated
    public static void attachBaseContext() {
        ARouter.attachBaseContext();
    }

    public static synchronized void monitorMode() {
        ARouter.monitorMode();
    }

    public static boolean isMonitorMode() {
        return ARouter.isMonitorMode();
    }

    public static void setLogger(ILogger userLogger) {
        ARouter.setLogger(userLogger);
    }

    /**
     * Inject params and services.
     */
    public void inject(Object obj) {
        aRouter.inject(obj);
    }

    /**
     * Build the roadmap, draw a postcard.
     *
     * @param path Where you go.
     */
    public Postcard build(String path) {
        if (TextUtils.isEmpty(path) || !path.startsWith(RouterPath.schemePathHead())) {
            return ARouter.getInstance().build(path);
        } else {
            return ARouter.getInstance().build(Uri.parse(path));
        }
        //return ARouter.getInstance().build(path);
    }

    /**
     * Build the roadmap, draw a postcard.
     *
     * @param path  Where you go.
     * @param group The group of path.
     */
    @Deprecated
    public Postcard build(String path, String group) {
        return ARouter.getInstance().build(path, group);
    }

    /**
     * Build the roadmap, draw a postcard.
     *
     * @param url the path
     */
    public Postcard build(Uri url) {
        return ARouter.getInstance().build(url);
    }

    /**
     * Launch the navigation by type
     *
     * @param service interface of service
     * @param <T>     return type
     * @return instance of service
     */
    public <T> T navigation(Class<? extends T> service) {
        return ARouter.getInstance().navigation(service);
    }

    /**
     * Launch the navigation.
     *
     * @param mContext    .
     * @param postcard    .
     * @param requestCode Set for startActivityForResult
     * @param callback    cb
     */
    public Object navigation(Context mContext, Postcard postcard, int requestCode, NavigationCallback callback) {
        return ARouter.getInstance().navigation(mContext, postcard, requestCode, callback);
    }


    public static class Build {

        private boolean openLog;
        private boolean openDebug;
        private boolean autoInitFlutterEngine = false;
        private Application application;
        private String scheme;
        private String host;
        private boolean openFlutterBoost;

        public Build openFlutterBoost(boolean openFlutterBoost) {
            this.openFlutterBoost = openFlutterBoost;
            return this;
        }

        public Build scheme(String scheme) {
            this.scheme = scheme;
            return this;
        }

        public Build host(String host) {
            this.host = host;
            return this;
        }

        public Build application(Application application) {
            this.application = application;
            return this;
        }

        public Build openLog(boolean openLog) {
            this.openLog = openLog;
            return this;
        }

        public Build openDebug(boolean openDebug) {
            this.openDebug = openDebug;
            return this;
        }

        public Build autoInitFlutterEngine(boolean autoInitFlutterEngine) {
            this.autoInitFlutterEngine = autoInitFlutterEngine;
            return this;
        }

        public void build() {
            instance = new JRouter(this);
        }

    }

    /**
     * @param context
     * @param routePath
     */
    public void navigator(Context context, String routePath) {
        if (routePath.contains(scheme) && routePath.contains(host)) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(routePath)));
        } else {
            Toast.makeText(context, "Not a valid routing path！", Toast.LENGTH_SHORT).show();
        }

    }
}
