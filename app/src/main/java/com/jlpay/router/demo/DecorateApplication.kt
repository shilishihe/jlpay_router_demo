package com.jlpay.router.demo

import android.app.Application
import android.content.Context
import lib.lh.router.JRouter


class DecorateApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        JRouter.Build()
            .application(this)
            .openDebug(true)
            .openLog(true)
            .openFlutterBoost(true)
            .host(BuildConfig.ROUTE_HOST)
            .scheme(BuildConfig.ROUTE_SCHEME)
            .build()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }


}