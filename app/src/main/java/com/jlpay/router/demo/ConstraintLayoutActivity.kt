package com.jlpay.router.demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import lib.lh.router.RouterPath

@Route(path = RouterPath.MODULE_DEMO_CONSTRAINT_LAYOUT_ACTIVITY)
class ConstraintLayoutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_constraint_layout)
    }
}