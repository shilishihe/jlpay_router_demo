package com.jlpay.router.demo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import lib.lh.router.JRouter
import lib.lh.router.RouterEntrance
import lib.lh.router.RouterPath

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.button_first).setOnClickListener {
            RouterEntrance.nav2ModuleJavaActivity(context)
        }
        view.findViewById<Button>(R.id.button_two).setOnClickListener {
            JRouter.instance().build(RouterPath.MODULE_ROUTER_H5_ACTIVITY)
                .withString("url", "file:///android_asset/scheme-test.html")
                .navigation(context)
        }
        view.findViewById<Button>(R.id.button_three).setOnClickListener {
            JRouter.instance().build(RouterPath.MODULE_KOTLIN_KOTLIN_ACTIVITY).navigation(context)
        }
        view.findViewById<Button>(R.id.button_four).setOnClickListener {
            JRouter.instance().build(RouterPath.MODULE_DEMO_USER_CENTER).navigation(context)
        }
        view.findViewById<Button>(R.id.button_five).setOnClickListener {
            val args = hashMapOf(
                "describe" to "Flutter Engine负责线程管理，Dart VM状态管理和Dart代码加载等工作。"
                , "name" to "阿里FlutterBoost框架"
            )
            JRouter.instance()
                .build(RouterPath.MODULE_FLUTTER_NEWS_CHINA_NEWS)
                .withObject(JRouter.FLUTTER_ROUTER_ARGUMENTS, args)
                .navigation(context)
        }

        view.findViewById<Button>(R.id.button_six).setOnClickListener {
            JRouter.instance()
                .build(RouterPath.MODULE_ROUTER_FLUTTER_DISPATCH + "99999")
                .navigation(context)
        }



        view.findViewById<Button>(R.id.button_constraint).setOnClickListener {
            JRouter.instance().build(RouterPath.MODULE_DEMO_CONSTRAINT_LAYOUT_ACTIVITY)
                .navigation(context)
        }

        view.findViewById<Button>(R.id.button_constraint).visibility = View.GONE
    }
}