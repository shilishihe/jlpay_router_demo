package com.jlpay.router.demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import lib.lh.router.RouterExtrasFlag
import lib.lh.router.RouterPath

// 个人中心页面，声明了进入该页面时候需要登录
//  注意这里的extras可以用或运算
@Route(
    path = RouterPath.MODULE_DEMO_USER_CENTER,
    extras = RouterExtrasFlag.FLAG_LOGIN
)
class UserCenterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_center)
    }
}