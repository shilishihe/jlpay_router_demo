package com.jlpay.router.demo

import android.content.Context
import com.alibaba.android.arouter.facade.Postcard
import com.alibaba.android.arouter.facade.annotation.Interceptor
import com.alibaba.android.arouter.facade.callback.InterceptorCallback
import com.alibaba.android.arouter.facade.template.IInterceptor
import com.alibaba.android.arouter.launcher.ARouter
import lib.lh.router.RouterExtrasFlag
import lib.lh.router.RouterPath

// 业务拦截器
@Interceptor(priority = 2, name = "业务路由拦截器")
class BusinessRouterIntercepter : IInterceptor {

    lateinit var context: Context

    // 此方法运行在线程池中的子线程中
    override fun process(postcard: Postcard, callback: InterceptorCallback) {
        var routerFlage = postcard.extra
        var flag = routerFlage.and(RouterExtrasFlag.FLAG_LOGIN)
        if (flag != 0) {
            callback.onInterrupt(RuntimeException("需要登录操作"))
            ARouter.getInstance().build(RouterPath.MODULE_DEMO_USER_LOGIN).navigation(this.context)
        }
        callback.onContinue(postcard)
    }

    override fun init(context: Context) {
        this.context = context
    }
}